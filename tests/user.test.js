const request = require('supertest');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const app = require('../app');
const User = require('../src/models/user.model');

const id = new mongoose.Types.ObjectId();
const userOne = {
    _id: id,
    email: 'mahjoubi.sfx@gmail.com',
    password: 'superAdmin',
    token: jwt.sign({_id: id}, 'nodeJs')
};

beforeEach(async () => {
    await User.deleteMany();
    await new User(userOne).save();
});


test('create new user', async () => {
    await request(app)
        .post('/api/users')
        .set('Authorization', `Bearer ${userOne.token}`)
        .send({
            email: userOne.email,
            password: userOne.password,
        })
        .expect(201);
});

test('user loggin', async () => {
    await request(app)
        .post('/api/users/login')
        .send({
            email: userOne.email,
            password: userOne.password,
        })
        .expect(200);
});

test('user login fail', async () => {
    await request(app)
        .post('/api/users/login')
        .send({
            email: userOne.email,
            password: 'thisIsNotThePass',
        })
        .expect(500);
});
