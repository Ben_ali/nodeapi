const jwt = require('jsonwebtoken');
const User = require('../models/user.model');

const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const decode = jwt.verify(token, 'nodeJs');
        const user = await User.findOne({_id: decode._id, 'token': token});
        if(!user) {
            throw new Error('No user exist')
        }
        req.token = token;
        req.user = user;
        next();
    } catch (e) {
        res.status(401).send({error: 'authentication required'})
    }
};

module.exports = auth;
