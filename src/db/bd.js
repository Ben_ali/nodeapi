const mongoose = require('mongoose');

const connectionURL = process.env.MONGODB;

mongoose.connect(connectionURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
});

