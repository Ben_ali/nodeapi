require('babel-register');
const User = require('../models/user.model');

const findAllUsers = (req, res) => {
    User.find({}).then((users) => {
        res.status(200).send(users)
    }).catch(err => {
        console.log(err.message);
        res.status(400).send(err)
    });
};
const addUser = (req, res) => {
    console.log(req.body);
    const user = new User(req.body);
    user.save()
        .then((user) => {
            console.log(user);
            res.status(201).send(user)
        })
        .catch(err => {
            console.log(err.message);
            res.status(400).send(err)
        });
};

const findUserById = (req, res) => {
    User.findById(req.params.id).then((user) => {
        if (!user) {
            return res.status(404).send();
        }
        res.status(200).send(user);
    }).catch(err => {
        console.log(err.message);
        res.status(400).send(err)
    });

};

const findCurrentUser = (req, res) => {
    console.log(req);
    res.send(req.user);

};

const updateUser = (req, res) => {
    console.log(req.params.id, req.body);
    User.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true}).then((user) => {
        if (!user) {
            return res.status(404).send();
        }
        res.status(200).send(user);
    }).catch(err => {
        console.log(err.message);
        res.status(400).send(err)
    });

};

const login = async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        const token = await user.authWithToken();
        res.send({user, token})

    } catch (e) {
        res.status(500).send();
    }
};

const logout = async (req, res) => {
    try {
        req.user.token = null;
        console.log(req.user.tokens);
        await req.user.save();

        res.send()
    } catch (e) {
        res.status(500).send();
    }
};

module.exports = {
    findAllUsers,
    findUserById,
    addUser,
    updateUser,
    login,
    logout,
    findCurrentUser
};
