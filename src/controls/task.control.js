const Task = require('../models/task.model');


const findAlltasks = (req, res) => {
    Task.find({}).then((tasks) =>  {
        res.status(200).send(tasks)
    }).catch(err => {
        console.log(err.message);
        res.status(400).send(err)
    });
};

const findtaskById = (req, res) => {
    Task.findById(req.params.id).then((task) =>  {
        if(!task) {
            return res.status(404).send();
        }
        res.status(200).send(task);
    }).catch(err => {
        console.log(err.message);
        res.status(400).send(err)
    });

};
const addtask = (req, res) => {
    console.log(req.body);
    const task = new Task(req.body);
    task.save()
        .then((task) => {
            console.log(task);
            res.status(201).send(task)
        })
        .catch(err => {
            console.log(err.message);
            res.status(400).send(err)
        });
};
const updatetask = (req, res) => {
    console.log(req.params.id, req.body);
    Task.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true}).then((task) =>  {
        if(!task) {
            return res.status(404).send();
        }
        res.status(200).send(task);
    }).catch(err => {
        console.log(err.message);
        res.status(400).send(err);
    });
};


module.exports = {findAlltasks, findtaskById, addtask, updatetask};
