const mongoose = require('mongoose');

const Task = mongoose.model('task',{
    title: {
        require: true,
        type: String,
        trim: true
    },
    description: {
        type: String,
        trim: true,
        maxLength: 255
    },
    inProgress: {
        type: Boolean,
        trim: true
    },
} );

module.exports = Task;
