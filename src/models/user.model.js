const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        trim: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    password: {
        type: String,
        minlength: 7,
        trim: true,
        validate(value) {
            if (value.toLowerCase().includes('password')) {
                throw new Error('Password cannot contain "password"')
            }
        }
    },
    token: {
        type: String,
    }
});

userSchema.methods.toJSON = function() {
    const user = this;
    const userObject = user.toObject();

    delete userObject.token;
    delete userObject.password;
    return userObject;
};

userSchema.methods.authWithToken = async function() {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString() }, 'nodeJs');
    console.log(user.token);
    user.token = token;
    await user.save();
    return token;
};
userSchema.statics.findByCredentials = async (email, password) => {
    const user =  await User.findOne({email});
    const isMatch = await bcrypt.compare(password, user.password);
    if (!user) {
        throw Error('no connection possible')
    }
    if (!isMatch) {
        throw Error('no connection possible')
    }
    return user
};


userSchema.pre('save', async function (next) {
    const user = this;
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8);
    }

    next();
});

const User = mongoose.model('User', userSchema);

module.exports = User;
