const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const userControl = require('../controls/user.control');

router.get('/api/users',auth ,  userControl.findAllUsers);

router.get('/api/users/:id', auth, userControl.findUserById);

router.get('/api/users/currentUser', auth, userControl.findCurrentUser);

router.patch('/api/users/:id', auth, userControl.updateUser);

router.post('/api/users', auth, userControl.addUser);

router.post('/api/users/logout', auth, userControl.logout);

router.post('/api/users/login', userControl.login);


module.exports = router;
