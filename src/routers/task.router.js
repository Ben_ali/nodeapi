
const express = require('express');
const router = express.Router();

const taskControl = require('../controls/task.control');

router.get('/api/tasks', taskControl.findAlltasks);

router.patch('/api/tasks/:id', taskControl.updatetask);

router.get('/api/tasks/:id', taskControl.findtaskById);

router.post('/api/tasks', taskControl.addtask);


module.exports = router;
