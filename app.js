require('./src/db/bd');

const express = require('express');
const bodyParser = require('body-parser');

const userRouter = require('./src/routers/user.router');
const taskRouter = require('./src/routers/task.router');

const  app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(userRouter);
app.use(taskRouter);

module.exports = app;

